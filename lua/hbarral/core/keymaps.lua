local function merge_tables(t1, t2)
  local result = {}
  for k, v in pairs(t1) do
    result[k] = v
  end
  for k, v in pairs(t2) do
    result[k] = v
  end
  return result
end

vim.g.mapleader = ","
vim.keymap.set("n", "<space>w", vim.cmd.w)

vim.keymap.set("v", "J", ":m '>+1<CR>gv=gv")
vim.keymap.set("v", "K", ":m '<-2<CR>gv=gv")

vim.keymap.set("v", "<", "<gv")
vim.keymap.set("v", ">", ">gv")

vim.keymap.set("n", "J", "mzJ`z")

vim.keymap.set("i", "jj", "<Esc>")
vim.keymap.set("i", "jk", "<Esc>l")
vim.keymap.set("i", "Jk", "<Esc>")
vim.keymap.set("i", "kk", "<Esc>l")

vim.keymap.set("n", "<C-d>", "<C-d>zz")
vim.keymap.set("n", "<C-u>", "<C-u>zz")

vim.keymap.set("n", "<Tab>", ":bnext<CR>")
vim.keymap.set("n", "<S-Tab>", ":bprevious<CR>")

vim.keymap.set("n", "n", "nzzzv")
vim.keymap.set("n", "N", "Nzzzv")

-- vim.keymap.set("x", "p", '"_dP')

vim.keymap.set("n", "<leader>y", '"+y')
vim.keymap.set("v", "<leader>y", '"+y')
vim.keymap.set("n", "<leader>Y", '"+Y')

vim.keymap.set("n", "Q", "<nop>")

vim.keymap.set("n", "<C-k>", "<cmd>cnext<CR>zz")
vim.keymap.set("n", "<C-j>", "<cmd>cprev<CR>zz")
vim.keymap.set("n", "<leader>k", "<cmd>lnext<CR>zz")
vim.keymap.set("n", "<leader>j", "<cmd>lprev<CR>zz")

vim.keymap.set("n", "<leader>rp", [[:%s/\<<C-r><C-w>\>/<C-r><C-w>/gI<Left><Left><Left>]])
vim.keymap.set("n", "<leader>x", "<cmd>!chmod +x %<CR>", { silent = true })

vim.keymap.set("n", "<Leader>h", ":<C-u>split<CR>")
vim.keymap.set("n", "<Leader>v", ":<C-u>vsplit<CR>")

vim.keymap.set("n", "<leader><space>", ":noh<CR>", { silent = true })

-- vim.keymap.set("n", "<leader>cc", ":Bdelete<CR>", { silent = true })
vim.keymap.set("n", "<leader>cc", ":bd<CR>", { silent = true })
vim.keymap.set("n", "<leader>ca", ":bufdo :Bdelete<CR>", { silent = true })

vim.keymap.set("n", "<C-J>", "<C-W>j")
vim.keymap.set("n", "<C-K>", "<C-W>k")
vim.keymap.set("n", "<C-L>", "<C-W>l")
vim.keymap.set("n", "<C-H>", "<C-W>h")

vim.keymap.set("n", "<leader>O", ":only<CR>")

-- run select text in terminal
vim.keymap.set("x", "<leader>r", "<esc>:'<,'>:w !sh<CR>")

-- edit vimrc
vim.keymap.set("n", "<leader>ev", ":e $MYVIMRC<CR>", { noremap = false })

-- edit bashrc
vim.keymap.set("n", "<leader>eb", ":e ~/.bashrc<CR>", { noremap = false })

-- vim.api.nvim_set_keymap("t", "<C-;>", "<C-\\><C-n>")

-- cheatsheet
vim.keymap.set(
  "n",
  "<leader>ch",
  ":FloatermNew --autoclose=0 --title=cheatsheet /home/hbarral/dotfiles/scripts/cht.sh<CR>"
)

vim.keymap.set("i", "<Esc>", "<Esc>")
vim.keymap.set("n", "<Esc>", "<Esc>")

-- Floaterm terminal
local opts = { noremap = true, silent = true }
-- autoclose=1
vim.keymap.set("n", "<C-t>", ":FloatermToggle<CR>", opts)
vim.keymap.set("t", "<C-t>", "<C-\\><C-n>:FloatermToggle<CR>", opts)
function load_floaterm_mappings()
  local buffer_name = vim.api.nvim_buf_get_name(0)
  local floaterm_title = vim.b.floaterm_title

  if vim.bo.buftype == "terminal" and floaterm_title and string.match(floaterm_title, "^HB") then
    vim.api.nvim_buf_set_keymap(0, "t", "<C-j>", [[<C-\><C-n>:FloatermPrev<CR>]], opts)
    vim.api.nvim_buf_set_keymap(0, "t", "<C-k>", [[<C-\><C-n>:FloatermNext<CR>]], opts)
    vim.api.nvim_buf_set_keymap(0, "t", "<C-h>", [[<C-\><C-n>:FloatermNew<CR>]], opts)
  end
end

vim.cmd([[
    augroup FloatermMappings
        autocmd!
        autocmd TermOpen term://* lua load_floaterm_mappings()
    augroup END
]])

-- AI
vim.keymap.set("n", "<C-g>", ":GpChatNew vsplit<CR>")

-- winresizer
vim.g.winresizer_start_key = "<space>r"

-- Telescope
-- vim.keymap.set("n", "<leader>ff", function() require("telescope.builtin").find_files() end, merge_tables(opts, { desc = "Fuzzy find files in cwd" }))
-- -- vim.keymap.set("n", "<leader>fw", function() require("telescope.builtin").live_grep() end, merge_tables(opts, { desc = "Find string in cwd" }))
-- -- vim.keymap.set("n", "<leader>fw", function() require("telescope.extensions").live_grep_args.live_grep_args() end, merge_tables(opts, { desc = "Find string in cwd" }))
-- vim.keymap.set("n", "<leader>fw", function() require("telescope").extensions.live_grep_args.live_grep_args() end)
-- -- vim.keymap.set("n", "<leader>fw", function()
-- --     require('telescope.builtin').live_grep({
-- --         fuzzy = true,
-- --         only_sort_text = true,
-- --         additional_args = function()
-- --             return {"--pcre2"}
-- --         end
-- --     })
-- -- end, { desc = "Find string in cwd" })
-- vim.keymap.set("n", "<leader>fg", function() require("telescope.builtin").git_files() end, merge_tables(opts, { desc = "Find files within the repository" }))
-- vim.keymap.set("n", "<leader>fb", function() require("telescope.builtin").buffers() end, merge_tables(opts, { desc = "Find in buffers" }))
-- vim.keymap.set("n", "<leader>b", function() require("telescope.builtin").buffers() end, merge_tables(opts, { desc = "Find in buffers" }))
-- vim.keymap.set("n", "<leader>fh", function() require("telescope.builtin").help_tags() end, merge_tables(opts, { desc = "Find within tags" }))
-- vim.keymap.set("n", "<leader>fc", function() require("telescope.builtin").grep_string() end, merge_tables(opts, { desc = "Find string under de cursor in cwd" }))
-- vim.keymap.set("n", "<leader>f", function() require("telescope.builtin").find_files() end, merge_tables(opts, {})) -- legacy keybinding
-- vim.keymap.set("n", "?", function() require("telescope.builtin").current_buffer_fuzzy_find() end, merge_tables(opts, { desc = "Search lines" }))

-- FZF
-- vim.keymap.set("n", "<leader>fb", "<cmd>Buffers<CR>", merge_tables(opts, { desc = "Open buffers " }))
-- vim.keymap.set("n", "<leader>b", "<cmd>Buffers<CR>", merge_tables(opts, { desc = "Open buffers " }))
-- vim.keymap.set("n", "?", "<cmd>BLines<CR>", merge_tables(opts, { desc = "Search lines" }))
-- vim.keymap.set("n", "<leader>ff", "<cmd>Files<CR>", merge_tables(opts, { desc = "Fuzzy find files in cwd" }))
-- vim.keymap.set("n", "<leader>fw", "<cmd>RG<CR>", merge_tables(opts, { desc = "Find string in cwd" }))

-- FZF-lua
-- stylua: ignore start
vim.keymap.set("n", "<leader>ff", function() require("fzf-lua").files() end, merge_tables(opts, { desc = "Fuzzy find files in cwd" }))
vim.keymap.set("n", "<leader>fg", function() require("fzf-lua").live_grep() end, merge_tables(opts, { desc = "Find by grepping current working directory" }))
vim.keymap.set("n", "<leader>fb", function() require("fzf-lua").buffers() end, merge_tables(opts, { desc = "Open buffers " }))
-- vim.keymap.set("n", "<leader>b", function() require("fzf-lua").buffers() end, merge_tables(opts, { desc = "Open buffers " }))
vim.keymap.set("n", "<leader>fw", function() require("fzf-lua").grep_cword() end, merge_tables(opts, { desc = "Find string in cwd" }))
vim.keymap.set("n", "<leader>fc", function()  require("fzf-lua").files({ cwd=vim.fn.stdpath("config")}) end, merge_tables(opts, { desc= "Find in neovim configuration"}))
vim.keymap.set("n", "?b", function()  require("fzf-lua").blines() end, merge_tables(opts, { desc= "Find in neovim configuration"}))
vim.keymap.set("n", "?a", function()  require("fzf-lua").lines() end, merge_tables(opts, { desc= "Find in neovim configuration"}))
-- 
-- stylua: ignore end

-- LSP
vim.api.nvim_set_keymap("n", "<leader>ft", ":lua vim.lsp.buf.format({ async = true })<CR>", opts)
-- vim.cmd [[ command! -buffer LspCodeAction lua vim.lsp.buf.code_action() ]]
vim.keymap.set("n", "<leader>d", ":lua vim.diagnostic.open_float()<CR>", { desc = "Show buffer diagnostics" })
-- keymap.set("n", "<leader>D", "<cmd>Telescope diagnostics bufnr=0<CR>", opts)

-- GitGutter
vim.keymap.set("n", "]c", ":GitGutterNextHunk<CR>", { desc = "Next git hunk" })
vim.keymap.set("n", "[c", ":GitGutterPrevHunk<CR>", { desc = "Previous git hunk" })
vim.keymap.set("n", "<leader>GG", ":GitGutterToggle<CR>", { desc = "Toggle git gutter" })
vim.keymap.set("n", "<leader>GH", ":GitGutterLineHighlightsToggle<CR>", { desc = "Toggle git gutter highlight" })

-- Noice
vim.keymap.set("n", "<space>d", "<cmd>Noice dismiss<CR>", merge_tables(opts, { desc = "Noice Dismiss" }))

-- Tagbar
vim.keymap.set("n", "<F2>", "<cmd>TagbarToggle<CR>", { desc = "Toggle tagbar" })

-- Avante
vim.keymap.set("n", "<leader>aa", ":lua require('avante').toggle()<CR>", { desc = "Toggle Avante" })

-- Maximizer
-- maximize current split or return to previous
-- noremap <C-w>m :MaximizerToggle<CR>
vim.keymap.set("n", "<C-w>m", ":MaximizerToggle<CR>", { desc = "Toggle Maximizer" })

-- File Manager
-- local api = require("nvim-tree.api")
-- local function opts(desc)
--   return {
--     desc = "nvim-tree: " .. desc,
--     buffer = bufnr,
--     noremap = true,
--     silent = true,
--     nowait = true,
--   }
-- end

-- api.config.mappings.default_on_attach(bufnr)

-- vim.keymap.set("n", "l", api.node.open.edit, opts("Open"))
-- vim.keymap.set("n", "h", api.node.navigate.parent_close, opts("Close Directory"))
-- vim.keymap.set("n", "v", api.node.open.vertical, opts("Open: Vertical Split"))
-- vim.keymap.del("n", "<C-k>", { buffer = bufnr })
-- vim.keymap.set("n", "<S-k>", api.node.open.preview, opts("Open Preview"))
-- vim.keymap.set("n", "N", ":lua require('nvim-tree.api').nvim-tree-api.fs.create()<CR>", opts)
-- vim.keymap.set("n", "yy", ":lua require('nvim-tree.api').nvim-tree-api.fs.copy.absolute_path()<CR>", opts)
