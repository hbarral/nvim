-- Automatically delete the contents of the nvim log file on startup
vim.api.nvim_create_autocmd("VimEnter", {
  callback = function()
    local file = io.open(vim.fn.expand("~/.local/state/nvim/lsp.log"), "w")
    if file then
      file:close()
    end
  end
})

vim.api.nvim_create_autocmd({ "BufWritePre" }, { pattern = { "*.templ" }, callback = vim.lsp.buf.format })
