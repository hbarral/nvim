vim.cmd([[
  function! ConvertJsonToString()
    normal! gv
    '<,'>!jq -c -M '. | @json'
  endfunction

  nnoremap <leader>js :call ConvertJsonToString()<CR>
]])

vim.cmd("command! JSONify %!jq -cr")
