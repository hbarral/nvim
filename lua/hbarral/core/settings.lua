vim.opt.number = false
vim.opt.relativenumber = false
vim.opt.numberwidth = 4
vim.opt.signcolumn = "yes"
vim.opt.tabstop = 2
vim.opt.softtabstop = 0
vim.opt.expandtab = true
vim.opt.shiftwidth = 2

vim.opt.wrap = false

vim.opt.hlsearch = true
vim.opt.incsearch = true
vim.opt.ignorecase = true
vim.opt.smartcase = true
-- vim.opt.nobackup = true
-- vim.opt.noswapfile = true

vim.opt.termguicolors = true
-- vim.opt.guifont = "Fira Code:16"
vim.opt.title = false

vim.opt.fillchars = vim.opt.fillchars + "eob: "
vim.opt.fillchars:append({
  vert = "│",
  stl = " ",
})

vim.opt.sidescrolloff = 8
vim.opt.scrolloff = 0
vim.opt.isfname:append("@-@")

vim.opt.updatetime = 50

-- vim.opt.colorcolumn = "80"

vim.opt.mouse = ""

vim.opt.clipboard = "unnamedplus"

vim.opt.cmdheight = 2
vim.opt.completeopt = { "menu", "menuone", "noinsert", "noselect" }
vim.opt.conceallevel = 0
vim.opt.pumheight = 10
vim.opt.pumblend = 10
vim.opt.showmode = false
vim.opt.showtabline = 0
vim.opt.smartindent = true
vim.opt.splitbelow = true
vim.opt.splitright = true
vim.opt.swapfile = false
vim.opt.timeoutlen = 1000
vim.opt.undofile = true
vim.opt.writebackup = false
vim.opt.cursorline = false
vim.opt.laststatus = 3
vim.opt.showcmd = false
vim.opt.ruler = false
-- vim.opt.shortmess:append("c")
vim.opt.shortmess = vim.opt.shortmess + { c = true, I = true, W = true }

-- vim.cmd "set whichwrap+=<,>,[,],h,l"
-- vim.cmd [[set iskeyword+=-]]

-- vim.g.netrw_banner = 0
-- vim.g.netrw_mouse = 2

-- Ignore whitespace in diff
local diffopt = vim.opt.diffopt:get()
if not vim.tbl_contains(diffopt, "iwhiteall") then
  table.insert(diffopt, "iwhiteall")
  vim.opt.diffopt = diffopt
end

-- spell
vim.opt.spell = false
vim.opt.spelllang = { "en_us", "es_es" }

vim.filetype.add({ extension = { templ = "templ" } })
