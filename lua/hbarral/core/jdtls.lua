---@diagnostic disable: undefined-global
-- Dependencies:
-- - mason-registry
-- - nvim-jdtls
-- - nvim-dap
-- - nvim-cmp
-- - lspconfig

local util = require("lspconfig/util")

local function get_jdtls()
  local home = os.getenv("HOME")
  local jdtls_path = home .. "/.local/share/nvim/mason/packages/jdtls"

  -- Verify JDTLS installation
  if vim.fn.isdirectory(jdtls_path) ~= 1 then
    vim.notify("ERROR: JDTLS not found. Please install it with :MasonInstall jdtls", vim.log.levels.ERROR)
    return nil, nil, nil
  end

  -- Get launcher jar
  local launcher = vim.fn.glob(jdtls_path .. "/plugins/org.eclipse.equinox.launcher_*.jar")
  -- print("Looking for launcher in: " .. jdtls_path .. "/plugins/org.eclipse.equinox.launcher_*.jar")
  -- print("Found launcher: " .. (launcher ~= "" and launcher or "not found"))

  if launcher == "" then
    -- print("ERROR: JDTLS launcher jar not found. Try reinstalling JDTLS")
    -- List available files in plugins directory
    -- print("Available files in plugins directory:")
    vim.fn.system({ "ls", "-l", jdtls_path .. "/plugins/" })
    return nil, nil, nil
  end

  -- Get OS-specific config
  local config = jdtls_path .. "/config_linux"

  -- Debug information
  -- print("Checking config path: " .. config)
  -- print("Directory exists: " .. tostring(vim.fn.isdirectory(config) == 1))
  -- print("Directory permissions: ")
  -- vim.fn.system({ "ls", "-ld", config })

  -- Verify config directory exists and is accessible
  if vim.fn.isdirectory(config) ~= 1 then
    -- print("ERROR: JDTLS config directory not found at: " .. config)
    return nil, nil, nil
  end

  -- Verify config directory is readable
  if vim.fn.filereadable(config .. "/config.ini") ~= 1 then
    -- print("ERROR: JDTLS config.ini not found or not readable")
    return nil, nil, nil
  end

  -- Setup lombok
  local lombok = home .. "/.local/share/eclipse/lombok.jar"
  if vim.fn.filereadable(lombok) ~= 1 then
    -- Create eclipse directory if it doesn't exist
    vim.fn.mkdir(home .. "/.local/share/eclipse", "p")
    -- Download lombok if it doesn't exist
    local lombok_url = "https://projectlombok.org/downloads/lombok.jar"
    -- print("Downloading lombok from " .. lombok_url)
    vim.fn.system({ "curl", "-Lo", lombok, lombok_url })

    if vim.fn.filereadable(lombok) ~= 1 then
      -- print("ERROR: Failed to download lombok.jar")
      return nil, nil, nil
    end
  end

  return launcher, config, lombok
end

local function get_bundles()
  local home = os.getenv("HOME")
  local java_debug_path = home .. "/.local/share/nvim/mason/packages/java-debug-adapter"
  local java_test_path = home .. "/.local/share/nvim/mason/packages/java-test"

  -- Verify paths exist
  if vim.fn.isdirectory(java_debug_path) ~= 1 then
    vim.notify("java-debug-adapter not found. Installing...", vim.log.levels.WARN)
    vim.cmd("MasonInstall java-debug-adapter")
  end

  if vim.fn.isdirectory(java_test_path) ~= 1 then
    vim.notify("java-test not found. Installing...", vim.log.levels.WARN)
    vim.cmd("MasonInstall java-test")
  end

  local bundles = {}

  -- Add debug adapter
  local debug_jar = vim.fn.glob(java_debug_path .. "/extension/server/com.microsoft.java.debug.plugin-*.jar", 1)
  if debug_jar ~= "" then
    table.insert(bundles, debug_jar)
  end

  -- Add test jars
  local test_jars = vim.split(vim.fn.glob(java_test_path .. "/extension/server/*.jar", 1), "\n")
  for _, jar in ipairs(test_jars) do
    if jar ~= "" then
      table.insert(bundles, jar)
    end
  end

  return bundles
end

local function get_workspace()
  local home = os.getenv("HOME")
  local workspace_path = home .. "/.local/share/eclipse"

  -- Ensure base workspace directory exists
  vim.fn.mkdir(workspace_path, "p")

  -- Use the git root or current directory name for the workspace
  local root = util.find_git_ancestor(vim.fn.expand("%:p:h"))
  if not root then
    -- print("WARNING: No git root found, using current directory")
    root = vim.fn.getcwd()
  end

  local project_name = vim.fn.fnamemodify(root, ":t")
  local workspace_dir = workspace_path .. "/" .. project_name

  -- Only clean workspace if explicitly requested
  if vim.g.java_clean_workspace then
    if vim.fn.isdirectory(workspace_dir) == 1 then
      vim.notify("Cleaning Java workspace: " .. workspace_dir, vim.log.levels.INFO)
      vim.fn.delete(workspace_dir, "rf")
    end
  end

  -- Create workspace if it doesn't exist
  if vim.fn.isdirectory(workspace_dir) ~= 1 then
    vim.fn.mkdir(workspace_dir, "p")
    vim.notify("Created Java workspace: " .. workspace_dir, vim.log.levels.INFO)
  end

  -- print("Created workspace directory: " .. workspace_dir)
  return workspace_dir
end

local function java_keymaps()
  -- Allow yourself to run JdtCompile as a Vim command
  vim.cmd(
    "command! -buffer -nargs=? -complete=custom,v:lua.require'jdtls'._complete_compile JdtCompile lua require('jdtls').compile(<f-args>)"
  )
  -- Allow yourself/register to run JdtUpdateConfig as a Vim command
  vim.cmd("command! -buffer JdtUpdateConfig lua require('jdtls').update_project_config()")
  -- Allow yourself/register to run JdtBytecode as a Vim command
  vim.cmd("command! -buffer JdtBytecode lua require('jdtls').javap()")
  -- Allow yourself/register to run JdtShell as a Vim command
  vim.cmd("command! -buffer JdtJshell lua require('jdtls').jshell()")
  -- Allow yourself/register to run JdtCodeAction as a Vim command
  vim.cmd("command! -buffer JdtCodeAction lua vim.lsp.buf.code_action()")
  -- Allow yourself/register to run JdtOrganizeImports as a Vim command
  vim.cmd("command! -buffer JdtOrganizeImports lua require('jdtls').organize_imports()")
  -- Allow yourself/register to run JdtExtractVariable as a Vim command
  vim.cmd("command! -buffer JdtExtractVariable lua require('jdtls').extract_variable()")
  -- Allow yourself/register to run JdtExtractConstant as a Vim command
  vim.cmd("command! -buffer JdtExtractConstant lua require('jdtls').extract_constant()")
  -- Allow yourself/register to run JdtTestNearestMethod as a Vim command
  vim.cmd("command! -buffer JdtTestNearestMethod lua require('jdtls').test_nearest_method()")

  -- Set a Vim motion to <Leader> + j + o to organize imports in normal mode
  vim.keymap.set( "n", "<leader>jo", "<Cmd> lua require('jdtls').organize_imports()<CR>", { desc = "[j]ava [o]rganize imports" })

  -- Set a Vim motion to <Leader> + j + v to extract the code under the cursor to a variable
  vim.keymap.set( "n", "<leader>jv", "<Cmd> lua require('jdtls').extract_variable()<CR>", { desc = "[j]ava extract [v]ariable" })

  -- Set a Vim motion to <Leader> + j + c to extract the code under the cursor to a static variable
  vim.keymap.set( "n", "<leader>jc", "<Cmd> lua require('jdtls').extract_constant()<CR>", { desc = "[j]ava extract [c]onstant" })

  -- Set a Vim motion to <Leader> + j + t to run the test method currently under the cursor
  vim.keymap.set( "n", "<leader>jt", "<Cmd> lua require('jdtls').test_nearest_method()<CR>", { desc = "[j]ava [t]est Method" })

  -- Set a Vim motion to <Leader> + j + T to run an entire test suite (class)
  vim.keymap.set("n", "<leader>jT", "<Cmd> lua require('jdtls').test_class()<CR>", { desc = "[j]ava [T]est class" })

  -- Set a Vim motion to <Leader> + j + u to update the project configuration
  vim.keymap.set("n", "<leader>ju", "<Cmd> JdtUpdateConfig<CR>", { desc = "[j]ava [u]pdate config" })

  -- JDTLS
  vim.keymap.set("n", "<leader>ja", "<cmd>lua vim.lsp.buf.code_action()<CR>", { desc = "[j]ava [a]ctions" })

  -- Go to definition
  vim.keymap.set("n", "gd", "<cmd>lua vim.lsp.buf.definition()<CR>", { buffer = true, desc = "[G]oto [D]efinition" })
  vim.keymap.set("n", "gr", "<cmd>lua vim.lsp.buf.references()<CR>", { buffer = true, desc = "[G]oto [R]eferences" })
  vim.keymap.set( "n", "gi", "<cmd>lua vim.lsp.buf.implementation()<CR>", { buffer = true, desc = "[G]oto [I]mplementation" })
  vim.keymap.set( "n", "gt", "<cmd>lua vim.lsp.buf.type_definition()<CR>", { buffer = true, desc = "[G]oto [T]ype Definition" })
end

local function setup_jdtls()
  local home = os.getenv("HOME")
  local JAVA_8 = "8.0.432-amzn"
  local JAVA_11 = "11.0.25-amzn"
  local JAVA_17 = "17.0.13-amzn"
  local JAVA_21 = "21.0.5-amzn"

  -- Java versions configuration
  local java_versions = {
    ["8"] = "8.0.432-amzn",
    ["11"] = "11.0.25-amzn",
    ["17"] = "17.0.13-amzn",
    ["21"] = "21.0.5-amzn",
  }

  -- Verify Java installations
  for version, release in pairs(java_versions) do
    local java_path = home .. "/.sdkman/candidates/java/" .. release
    if vim.fn.isdirectory(java_path) ~= 1 then
      vim.notify(string.format("Java %s (%s) not found at %s", version, release, java_path), vim.log.levels.WARN)
    end
  end

  -- Get access to the jdtls plugin and all of its functionality
  local jdtls = require("jdtls")

  -- Get the paths to the jdtls jar, operating specific configuration directory, and lombok jar
  local launcher, os_config, lombok = get_jdtls()

  -- Get the path you specified to hold project information
  local workspace_dir = get_workspace()

  -- Get the bundles list with the jars to the debug adapter, and testing adapters
  local bundles = get_bundles()

  -- Determine the root directory of the project by looking for these specific markers
  -- Get the directory of the current file
  local file_dir = vim.fn.expand("%:p:h")
  local root_dir = util.root_pattern("pom.xml", "build.gradle", ".git")(file_dir)

  if not root_dir then
    root_dir = util.find_git_ancestor(file_dir)
  end

  if not root_dir then
    root_dir = file_dir
  end

  -- print("File directory: " .. file_dir)
  -- print("Found root directory: " .. (root_dir or "nil"))

  -- Ensure root_dir is valid
  if not root_dir or root_dir == "" then
    -- print("ERROR: Invalid root directory")
    return nil
  end

  -- Verify that this is a Java project directory
  local is_java_project = false
  local project_files = { "pom.xml", "build.gradle", "gradlew", "mvnw", ".git" }

  for _, file in ipairs(project_files) do
    if vim.fn.filereadable(root_dir .. "/" .. file) == 1 then
      is_java_project = true
      break
    end
  end

  if not is_java_project then
    -- print("ERROR: Not a Java project directory (missing pom.xml, build.gradle, etc.)")
    return nil
  end

  -- Tell our JDTLS language features it is capable of
  local capabilities = {
    workspace = {
      configuration = true,
    },
    textDocument = {
      completion = {
        snippetSupport = false,
      },
    },
  }

  local lsp_capabilities = require("cmp_nvim_lsp").default_capabilities()

  for k, v in pairs(lsp_capabilities) do
    capabilities[k] = v
  end

  -- Get the default extended client capablities of the JDTLS language server
  local extendedClientCapabilities = jdtls.extendedClientCapabilities
  -- Modify one property called resolveAdditionalTextEditsSupport and set it to true
  extendedClientCapabilities.resolveAdditionalTextEditsSupport = true

  -- Set the command that starts the JDTLS language server jar
  local cmd = {
    home .. "/.sdkman/candidates/java/" .. JAVA_21 .. "/bin/java",
    -- JVM options
    "-Xms1g",
    "-Xmx2g",
    "-XX:+UseParallelGC",
    "-XX:GCTimeRatio=4",
    "-XX:AdaptiveSizePolicyWeight=90",
    -- Eclipse options
    "-Declipse.application=org.eclipse.jdt.ls.core.id1",
    "-Dosgi.bundles.defaultStartLevel=4",
    "-Declipse.product=org.eclipse.jdt.ls.core.product",
    "-Dlog.protocol=true",
    "-Dlog.level=ALL",
    -- Java modules
    "--add-modules=ALL-SYSTEM",
    "--add-opens",
    "java.base/java.util=ALL-UNNAMED",
    "--add-opens",
    "java.base/java.lang=ALL-UNNAMED",
    "--add-opens",
    "java.base/java.lang.reflect=ALL-UNNAMED",
    "--add-opens",
    "java.base/java.io=ALL-UNNAMED",
    "--add-opens",
    "java.base/java.nio=ALL-UNNAMED",
    "--add-opens",
    "java.base/java.net=ALL-UNNAMED",
    "--add-opens",
    "java.base/java.util.concurrent=ALL-UNNAMED",
    "--add-exports",
    "jdk.compiler/com.sun.tools.javac.api=ALL-UNNAMED",
    "--add-exports",
    "jdk.compiler/com.sun.tools.javac.file=ALL-UNNAMED",
    "--add-exports",
    "jdk.compiler/com.sun.tools.javac.main=ALL-UNNAMED",
    "--add-exports",
    "jdk.compiler/com.sun.tools.javac.model=ALL-UNNAMED",
    "--add-exports",
    "jdk.compiler/com.sun.tools.javac.parser=ALL-UNNAMED",
    "--add-exports",
    "jdk.compiler/com.sun.tools.javac.processing=ALL-UNNAMED",
    "--add-exports",
    "jdk.compiler/com.sun.tools.javac.tree=ALL-UNNAMED",
    "--add-exports",
    "jdk.compiler/com.sun.tools.javac.util=ALL-UNNAMED",
    "-javaagent:" .. lombok,
    "-jar",
    launcher,
    "-configuration",
    os_config,
    "-data",
    workspace_dir,
  }

  -- Configure settings in the JDTLS server
  local settings = {
    java = {
      -- Enable code formatting
      format = {
        enabled = true,
        -- Use the Google Style guide for code formattingh
        settings = {
          -- Use Google Java style guidelines for formatting
          -- mkdir -p ~/.local/share/eclipse
          -- curl -o ~/.local/share/eclipse/eclipse-java-google-style.xml https://github.com/google/styleguide/blob/gh-pages/eclipse-java-google-style.xml
          url = vim.fn.expand(
            vim.g.java_format_settings_url or home .. "/.local/share/eclipse/eclipse-java-google-style.xml"
          ),
          profile = vim.g.java_format_settings_profile or "GoogleStyle",
        },
      },
      -- Enable downloading archives from eclipse automatically
      eclipse = {
        downloadSource = true,
      },
      -- Enable downloading archives from maven automatically
      maven = {
        downloadSources = true,
      },
      -- Enable method signature help
      signatureHelp = {
        enabled = true,
      },
      -- Use the fernflower decompiler when using the javap command to decompile byte code back to java code
      contentProvider = {
        preferred = "fernflower",
      },
      -- Setup automatical package import oranization on file save
      saveActions = {
        organizeImports = true,
      },
      -- Customize completion options
      completion = {
        -- When using an unimported static method, how should the LSP rank possible places to import the static method from
        favoriteStaticMembers = {
          "org.hamcrest.MatcherAssert.assertThat",
          "org.hamcrest.Matchers.*",
          "org.hamcrest.CoreMatchers.*",
          "org.junit.jupiter.api.Assertions.*",
          "java.util.Objects.requireNonNull",
          "java.util.Objects.requireNonNullElse",
          "org.mockito.Mockito.*",
        },
        -- Try not to suggest imports from these packages in the code action window
        filteredTypes = {
          "com.sun.*",
          "io.micrometer.shaded.*",
          "java.awt.*",
          "jdk.*",
          "sun.*",
        },
        -- Set the order in which the language server should organize imports
        importOrder = {
          "java",
          "jakarta",
          "javax",
          "com",
          "org",
        },
      },
      sources = {
        -- How many classes from a specific package should be imported before automatic imports combine them all into a single import
        organizeImports = {
          starThreshold = 9999,
          staticThreshold = 9999,
        },
      },
      -- How should different pieces of code be generated?
      codeGeneration = {
        -- When generating toString use a json format
        toString = {
          template = "${object.className}{${member.name()}=${member.value}, ${otherMembers}}",
        },
        -- When generating hashCode and equals methods use the java 7 objects method
        hashCodeEquals = {
          useJava7Objects = true,
        },
        -- When generating code use code blocks
        useBlocks = true,
      },
      -- If changes to the project will require the developer to update the projects configuration advise the developer before accepting the change
      configuration = {
        updateBuildConfiguration = "interactive",
        runtimes = {
          {
            name = "JavaSE-17",
            path = home .. "/.sdkman/candidates/java/" .. JAVA_17,
          },
          {
            name = "JavaSE-11",
            path = home .. "/.sdkman/candidates/java/" .. JAVA_11,
          },
          {
            name = "JavaSE-1.8",
            path = home .. "/.sdkman/candidates/java/" .. JAVA_8,
          },
          {
            name = "JavaSE-21",
            path = home .. "/.sdkman/candidates/java/" .. JAVA_21,
          },
        },
      },
      -- enable code lens in the lsp
      referencesCodeLens = {
        enabled = true,
      },
      -- enable inlay hints for parameter names,
      inlayHints = {
        parameterNames = {
          enabled = "all",
        },
      },
    },
  }

  -- Create a table called init_options to pass the bundles with debug and testing jar, along with the extended client capablies to the start or attach function of JDTLS
  local init_options = {
    bundles = bundles,
    extendedClientCapabilities = extendedClientCapabilities,
  }

  -- Function that will be ran once the language server is attached
  local on_attach = function(_, bufnr)
    -- Map the Java specific key mappings once the server is attached
    java_keymaps()

    -- Setup the java debug adapter of the JDTLS server
    require("jdtls.dap").setup_dap()

    -- Find the main method(s) of the application so the debug adapter can successfully start up the application
    -- Sometimes this will randomly fail if language server takes to long to startup for the project, if a ClassDefNotFoundException occurs when running
    -- the debug tool, attempt to run the debug tool while in the main class of the application, or restart the neovim instance
    -- Unfortunately I have not found an elegant way to ensure this works 100%
    require("jdtls.dap").setup_dap_main_class_configs()
    -- Enable jdtls commands to be used in Neovim
    require("jdtls.setup") -- .add_commands()
    -- Refresh the codelens
    -- Code lens enables features such as code reference counts, implemenation counts, and more.
    vim.lsp.codelens.refresh()

    -- Setup a function that automatically runs every time a java file is saved to refresh the code lens
    vim.api.nvim_create_autocmd("BufWritePost", {
      pattern = { "*.java" },
      callback = function()
        local _, _ = pcall(vim.lsp.codelens.refresh)
      end,
    })
  end

  -- Verify required files exist
  -- local required_files = {
  --   launcher,
  --   lombok,
  --   os_config,
  --   root_dir .. "/pom.xml", -- or build.gradle
  -- }

  -- for _, file in ipairs(required_files) do
  --   if vim.fn.filereadable(file) ~= 1 then
  --     print("ERROR: Required file not found: " .. file)
  --     return nil
  --   end
  -- end

  -- Print diagnostic information
  -- print("JDTLS Config:")
  -- print("Workspace: " .. workspace_dir)
  -- print("Root Dir: " .. (root_dir or "nil"))
  -- print("Launcher: " .. launcher)
  -- print("Lombok: " .. lombok)
  -- print("OS Config: " .. os_config)
  if not root_dir then
    -- print("ERROR: No root directory found. JDTLS will not start.")
    return nil
  end

  -- Ensure we have all required components
  if not (cmd and root_dir and settings and init_options and capabilities) then
    -- print("ERROR: Missing required configuration components")
    return nil
  end

  local config = {
    cmd = cmd,
    root_dir = root_dir,
    settings = settings,
    init_options = init_options,
    capabilities = capabilities,
    on_attach = on_attach,
    filetypes = { "java" },
  }

  -- Print final configuration for debugging
  -- print("Final config root_dir: " .. config.root_dir)

  return config
end

return {
  get_config = setup_jdtls,
}
