return {
  "folke/persistence.nvim",
  event = "BufReadPre",
  opts = {
    dir = vim.fn.stdpath("state") .. "/sessions/",
    options = { "buffers", "curdir", "tabpages", "winsize", "help", "globals", "skiprtp" },
    pre_save = nil,
    save_empty = false,
    need = 1,
    branch = true,
  },
  -- keys = {
  --   { "<leader>sc", function() require("persistence").load() end, desc = "Load session for current directory" },
  --   { "<leader>so", function() require("persistence").select() end, desc = "Select session to load" },
  --   { "<leader>sl", function() require("persistence").load({ last = true }) end, desc = "Load last session" },
  --   { "<leader>ss", function() require("persistence").stop() end, desc = "Stop Persistence" },
  -- },
}
