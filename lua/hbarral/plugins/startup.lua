return {
  "startup-nvim/startup.nvim",
  dependencies = {
    "nvim-telescope/telescope.nvim",
    "nvim-lua/plenary.nvim",
    "nvim-telescope/telescope-file-browser.nvim",
  },
  config = function()
    require("startup").setup({
      theme = "evil",
      options = {
        mapping_keys = true,
        cursor_column = 0.5,

        after = function() end,
        empty_lines_between_mappings = true,
        disable_statuslines = true,
        paddings = { 1, 2 },
      },
      mappings = {
        execute_command = "<CR>",
        open_file = "o",
        open_file_split = "<c-o>",
        open_section = "<TAB>",
        open_help = "?",
      },
      colors = {
        background = "#1f2227",
        folded_section = "#56b6c2",
      },
    })
  end,
}
