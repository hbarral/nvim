return {
  "airblade/vim-gitgutter",
  lazy = false,
  init = function()
    vim.g.gitgutter_enabled = true
    vim.g.gitgutter_map_keys = 0
    vim.g.gitgutter_sign_added = '│'
    vim.g.gitgutter_sign_modified = '│'
    vim.g.gitgutter_sign_removed = '_'
    vim.g.gitgutter_sign_removed_first_line = '‾'
    vim.g.gitgutter_sign_removed_above_and_below = '~'
    vim.g.gitgutter_sign_modified_removed = 'rr'
    -- vim.g.gitgutter_highlight_lines = true
    -- vim.g.gitgutter_highlight_linenrs = true
  end,
}
