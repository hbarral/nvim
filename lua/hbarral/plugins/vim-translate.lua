return {
	"VincentCordobes/vim-translate",
	config = function()
		local keymap = vim.keymap
		keymap.set("n", "<leader>tc", ":TranslateClear<CR>", { silent = true })
		keymap.set("v", "<leader>trr", ":TranslateReplace :ru<CR>", { silent = true })
		keymap.set("v", "<leader>trs", ":TranslateReplace :es<CR>", { silent = true })
		keymap.set("v", "<leader>tre", ":TranslateReplace :en<CR>", { silent = true })

		keymap.set("v", "<leader>tr", ":Translate :ru<CR>", { silent = true })
		keymap.set("v", "<leader>ts", ":Translate :es<CR>", { silent = true })
		keymap.set("v", "<leader>te", ":Translate :en<CR>", { silent = true })
	end,
}
