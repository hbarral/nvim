return {
  'Exafunction/codeium.vim',
  config = function()
    local opts = { expr = true, silent = true }
    -- vim.g.codeium_disable_bindings = 1
    vim.g.codeium_enabled = true

    vim.g.codeium_filetypes = {
      bash = false,
      typescript = true,
      java = true,
      python = true,
      lua = true,
      markdown = false,
      go = true,
    }

    vim.keymap.set('i', '<C-l>', function() return vim.fn['codeium#Accept']() end, opts)
    vim.keymap.set('i', '<C-j>', function() return vim.fn['codeium#CycleCompletions'](1) end, opts)
    vim.keymap.set('i', '<C-k>', function() return vim.fn['codeium#CycleCompletions'](-1) end, opts)
    vim.keymap.set('i', '<C-h>', function() return vim.fn['codeium#Clear']() end, opts)
    -- vim.keymap.set('i', '<M-Bslash>', function() return vim.fn['codeium#Complete']() end, opts)
  end
}
