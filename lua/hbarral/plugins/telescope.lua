-- https://github.com/nvim-telescope/telescope.nvim
-- requires ripgrep
return {
  "nvim-telescope/telescope.nvim",
  branch = "0.1.x",
  dependencies = {
    "nvim-lua/plenary.nvim",
    { "nvim-telescope/telescope-fzf-native.nvim", build = "make" },
    "nvim-tree/nvim-web-devicons",
    "nvim-telescope/telescope-live-grep-args.nvim",
  },
  config = function()
    local telescope = require("telescope")
    -- local builtin = require("telescope.builtin")
    local actions = require("telescope.actions")
    -- local lga_actions = require("telescope-live-grep-args.actions")

    telescope.setup({
      extensions = {
        -- live_grep_args = {
        --   auto_quoting = true, -- enable/disable auto-quoting
        --   -- define mappings, e.g.
        --   mappings = { -- extend mappings
        --     i = {
        --       ["<C-k>"] = lga_actions.quote_prompt(),
        --       ["<C-i>"] = lga_actions.quote_prompt({ postfix = " --iglob " }),
        --       -- freeze the current list and start a fuzzy search in the frozen list
        --       ["<C-space>"] = lga_actions.to_fuzzy_refine,
        --     },
        --   },
        --   -- ... also accepts theme settings, for example:
        --   -- theme = "dropdown", -- use dropdown theme
        --   -- theme = { }, -- use own theme spec
        --   -- layout_config = { mirror=true }, -- mirror preview pane
        -- },
      },
      defaults = {
        hidden = true,
        no_ignore = false,
        case_mode = "ignore", -- "ignore", "smart", or "respect"
        color_devicons = true,
        fuzzy = true, -- Habilita búsqueda fuzzy por defecto
        generic_sorter = require("telescope.sorters").get_fzy_sorter(),
        file_sorter = require("telescope.sorters").get_fzy_sorter(),
        layout_config = {
          width = 0.8,
          horizontal = {
            preview_width = 0.6,
          },
        },
        -- path_display = {
        --   filename_first = {
        --     reverse_directories = true,
        --   },
        -- },
        --
        -- - `"truncate"`: Trunca las rutas largas
        -- - `"smart"`: Muestra rutas de manera inteligente, acortando partes del medio
        -- - `"tail"`: Muestra solo el nombre del archivo
        -- - `"absolute"`: Muestra la ruta completa
        -- - `"shorten"`: Acorta los nombres de directorio a su primera letra
        path_display = { "smart" },
        file_ignore_patterns = {
          ".git/",
          ".cache",
          "%.o",
          "%.a",
          "%.out",
          "%.class",
          "%.pdf",
          "%.mkv",
          "%.mp4",
          "%.zip",
          "%_templ.go",
        },
        mappings = {
          i = {
            ["<C-k>"] = actions.move_selection_previous,
            ["<C-j>"] = actions.move_selection_next,
            ["<C-q>"] = actions.send_selected_to_qflist + actions.open_qflist,
          },
          n = {
            ["<ESC>"] = actions.close,
            ["j"] = actions.move_selection_next,
            ["k"] = actions.move_selection_previous,
            ["q"] = actions.close,
          },
        },
        vimgrep_arguments = {
          "rg",
          "--hidden",
          "--color=never",
          "--no-heading",
          "--with-filename",
          "--line-number",
          "--column",
          "--smart-case",
          -- "--respect-gitignore", -- Respeta el .gitignore
          "--glob=!node_modules/",
          "--glob=!target/",
          "--glob=!dist/",
          "--glob=!dockerfiles/",
          "--glob=!ios/",
          "--glob=!android/",
          "--glob=!tags",
          "--glob=!vendor/",
          "--glob=!yarn.lock",
          "--glob=!package-lock.json",
          "--glob=!composer.lock",
          "--glob=!dockerfiles/postgresql_driver/",
          "--glob=!dockerfiles/deployments/",
          "--glob=!deleted/",
          "--glob=!logs",
        },
        extensions = {
          fzf = {
            fuzzy = true,
            override_generic_sorter = true,
            override_file_sorter = true,
            case_mode = "smart_case",
          },
        },
      },
      pickers = {
        buffers = {
          ignore_current_buffer = true,
          sort_lastused = true,
          sort_mru = true,
        },
        live_grep = {
          fuzzy = true,
          only_sort_text = true, -- Solo aplica fuzzy al texto, no a rutas
          additional_args = function()
            return {
              "--pcre2",
              "--engine=auto",
              "--smart-case",
              "--multiline",
              "--multiline-dotall",
            }
          end,
        },
      },
    })

    telescope.load_extension("fzf")

    vim.api.nvim_create_autocmd("FileType", {
      pattern = "TelescopeTesults",
      callback = function(ctx)
        vim.api.nvim_buf_call(ctx.buf, function()
          vim.fn.matchadd("TelescopeParent", "\t\t.*$")
          vim.api.nvim_set_hl(0, "TelescopeParent", { link = "Comment" })
        end)
      end,
    })
  end,
}
