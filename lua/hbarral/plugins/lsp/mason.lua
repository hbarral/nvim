return {
  "williamboman/mason-lspconfig.nvim",
  dependencies = {
    "williamboman/mason.nvim",
    "WhoIsSethDaniel/mason-tool-installer.nvim",
    "jay-babu/mason-nvim-dap.nvim",
  },
  config = function()
    local mason = require("mason")
    local mason_lspconfig = require("mason-lspconfig")
    local mason_tool_installer = require("mason-tool-installer")
    local mason_registry = require("mason-registry")
    local servers = {
      "lua_ls",
      "ts_ls",
      "html",
      "cssls",
      "tailwindcss",
      "lua_ls",
      "htmx",
      "emmet_ls",
      "vimls",
      "jsonls",
      "yamlls",
      "marksman",
      "volar",
      "sqlls",
      "gopls",
      -- "jdtls",
      "intelephense",
      "eslint",
      "docker_compose_language_service",
      "dockerls",
    }
    local mason_nvim_dap = require("mason-nvim-dap")

    mason_nvim_dap.setup({
      ensure_installed = { "java-debug-adapter", "java-test" },
    })

    mason.setup({
      ui = {
        icons = {
          package_installed = "✓",
          package_pending = "➜",
          package_uninstalled = "✗",
        },
      },
    })

    mason_lspconfig.setup({
      ensure_installed = servers,
      automatic_installation = true,
    })

    mason_tool_installer.setup({
      ensure_installed = {
        "prettier",
        "eslint_d",
        "nomicfoundation-solidity-language-server",
        "pyright",
        -- "mypy",
        -- "ruff",
        -- "black",
      },
    })

    local tools = { "stylua", "luacheck", "hadolint" }

    for _, tool in ipairs(tools) do
      local package = mason_registry.get_package(tool)
      if not package:is_installed() then
        package:install()
      end
    end
  end,
}
