return {
  "neovim/nvim-lspconfig",
  event = { "BufReadPre", "BufNewFile" },
  dependencies = {
    "hrsh7th/cmp-nvim-lsp",
    {
      "antosha417/nvim-lsp-file-operations",
      config = true,
    },
    {
      "folke/neodev.nvim",
    },
  },
  config = function()
    local lspconfig = require("lspconfig")
    local cmp_nvim_lsp = require("cmp_nvim_lsp")
    local keymap = vim.keymap
    local opts = { noremap = true, silent = true }
    local util = require("lspconfig/util")
    -- local jdtls_config = require("hbarral.core.jdtls").get_config()

    local on_attach = function(client, bufnr)
      opts.buffer = bufnr

      opts.desc = "Show LSP references"
      keymap.set("n", "gR", "<cmd>Telescope lsp_references<CR>", opts)

      opts.desc = "Go to declaration"
      keymap.set("n", "gD", vim.lsp.buf.declaration, opts)

      opts.desc = "Show LSP definitions"
      keymap.set("n", "gd", "<cmd>Telescope lsp_definitions<CR>", opts)

      opts.desc = "Show LSP implementations"
      keymap.set("n", "<leader>gi", vim.lsp.buf.implementation, opts)

      opts.desc = "Show LSP type definitions"
      keymap.set("n", "gt", "<cmd>Telescope lsp_type_definitions<CR>", opts)

      opts.desc = "See available code actions"
      keymap.set({ "n", "v" }, "<leader>qf", vim.lsp.buf.code_action, opts)

      opts.desc = "Smart rename"
      keymap.set("n", "<leader>rn", vim.lsp.buf.rename, opts)

      opts.desc = "Show buffer diagnostics"
      keymap.set("n", "<leader>D", vim.diagnostic.open_float, opts)
      -- keymap.set("n", "<leader>D", "<cmd>Telescope diagnostics bufnr=0<CR>", opts)

      -- opts.desc = "Show line diagnostics"
      -- keymap.set("n", "]d", vim.diagnostic.open_float, opts)

      opts.desc = "Go to previous diagnostic"
      keymap.set("n", "[d", vim.diagnostic.goto_prev, opts)

      opts.desc = "Go to next diagnostic"
      keymap.set("n", "]d", vim.diagnostic.goto_next, opts)

      opts.desc = "Show documentation for what is under cursor"
      keymap.set("n", "K", vim.lsp.buf.hover, opts)

      opts.desc = "Restart LSP"
      keymap.set("n", "<leader>rs", ":LspRestart<CR>", opts)

      opts.desc = "Format Code"
      keymap.set("n", "<space>f", vim.lsp.buf.format, opts)

      if client.name == "eslint" then
        client.server_capabilities.documentFormattingProvider = true
      elseif client.name == "ts_ls" then
        client.server_capabilities.documentFormattingProvider = false
      end

      -- if client.server_capabilities.documentFormattingProvider then
      --   vim.api.nvim_command([[augroup Format]])
      --   vim.api.nvim_command([[autocmd! * <buffer>]])
      --   vim.api.nvim_command([[autocmd BufWritePre <buffer> lua vim.lsp.buf.format()]])
      --   vim.api.nvim_command([[augroup END]])
      -- end
    end

    local capabilities = cmp_nvim_lsp.default_capabilities()
    local signs = { Error = " ", Warn = " ", Hint = "󰠠 ", Info = " " }
    for type, icon in pairs(signs) do
      local hl = "DiagnosticSign" .. type
      vim.fn.sign_define(hl, { text = icon, texthl = hl, numhl = "" })
    end

    lspconfig.html.setup({
      capabilities = capabilities,
      on_attach = on_attach,
      filetypes = { "html" },
    })

    lspconfig.ts_ls.setup({
      capabilities = capabilities,
      on_attach = on_attach,
      filetypes = { "typescript", "typescriptreact", "typescript.tsx", "javascriptreact" },
      cmd = { "typescript-language-server", "--stdio" },
    })

    lspconfig["cssls"].setup({
      capabilities = capabilities,
      on_attach = on_attach,
    })

    lspconfig.tailwindcss.setup({
      on_attach = on_attach,
      capabilities = capabilities,
      filetypes = {
        "templ",
        "astro",
        "javascript",
        "typescript",
        "react",
        "typescriptreact",
        "typescript.tsx",
        "javascriptreact",
      },
      settings = {
        tailwindCSS = {
          includeLanguages = {
            templ = "html",
          },
        },
      },
    })

    lspconfig["svelte"].setup({
      capabilities = capabilities,
      on_attach = on_attach,
    })

    lspconfig["emmet_ls"].setup({
      capabilities = capabilities,
      on_attach = on_attach,
      filetypes = { "html", "typescriptreact", "javascriptreact", "css", "sass", "scss", "less", "svelte" },
    })

    lspconfig.intelephense.setup({
      capabilities = capabilities,
      on_attach = on_attach,
      filetypes = { "php" },
    })

    lspconfig.lua_ls.setup({
      capabilities = capabilities,
      on_attach = on_attach,
      settings = { -- custom settings for lua
        Lua = {
          runtime = {
            version = "LuaJIT",
          },
          diagnostics = {
            globals = { "vim" },
          },
          workspace = {
            -- Make the server aware of Neovim runtime files
            library = vim.api.nvim_get_runtime_file("", true),
            checkThirdParty = false,
          },
          telemetry = {
            enable = false,
          },
        },
      },
    })

    lspconfig.gopls.setup({
      capabilities = capabilities,
      on_attach = on_attach,
      filetypes = { "go", "gomod", "gowork", "gotmpl" },
      cmd = { "gopls" },
      root_dir = util.root_pattern("go.work", "go.mod", ".git"),
      settings = {
        gopls = {
          completeUnimported = true,
          usePlaceholders = false,
          experimentalPostfixCompletions = true,
          gofumpt = true,
          staticcheck = true,
          analyses = {
            nilness = true,
            unusedparams = true,
            unusedwrite = true,
            useany = true,
          },
        },
      },
    })

    lspconfig.solidity.setup({
      capabilities = capabilities,
      on_attach = on_attach,
      filetypes = { "solidity" },
      cmd = { "nomicfoundation-solidity-language-server", "--stdio" },
      root_dir = util.root_pattern(".git", "package.json"),
      single_file_support = true,
    })

    lspconfig.pyright.setup({
      capabilities = capabilities,
      on_attach = on_attach,
      filetypes = { "python" },
    })

    -- Skip JDTLS setup here since it's handled by ftplugin/java.lua
    -- lspconfig.jdtls.setup(jdtls_config)

    -- lspconfig.eslint.setup({
    --   on_attach = function(client, bufnr)
    --     vim.api.nvim_create_autocmd("BufWritePre", {
    --       buffer = bufnr,
    --       command = "EslintFixAll",
    --     })
    --   end,
    -- })

    -- lspconfig.eslint.setup({
    --   capabilities = capabilities,
    --   on_attach = on_attach,
    --
    --   codeAction = {
    --     disableRuleComment = {
    --       enable = true,
    --       location = "separateLine",
    --     },
    --     showDocumentation = {
    --       enable = true,
    --     },
    --   },
    --   codeActionOnSave = {
    --     enable = false,
    --     mode = "all",
    --   },
    --   format = false,
    --   nodePath = "",
    --   onIgnoredFiles = "off",
    --   problems = {
    --     shortenToSingleLine = false,
    --   },
    --   packageManager = "npm",
    --   quiet = false,
    --   rulesCustomizations = {
    --     ["quotes"] = { "error", "single" },
    --     ["no-console"] = { "warn" },
    --   },
    --   run = "onType",
    --   useESLintClass = false,
    --   validate = "on",
    --   -- workingDirectory = {
    --   --   mode = "location",
    --   -- },
    -- })

    lspconfig.eslint.setup({
      cmd = { "/home/hbarral/.local/share/nvim/mason/bin/vscode-eslint-language-server", "--stdio" },
      filetypes = {
        "javascript",
        "javascriptreact",
        "javascript.jsx",
        "typescript",
        "typescriptreact",
        "typescript.tsx",
        "vue",
        "svelte",
        "astro",
      },
      root_dir = lspconfig.util.root_pattern(".eslintrc", ".eslintrc.json", ".eslintrc.js", "package.json", ".git"),
      -- handlers = {
      --   ["eslint/confirmESLintExecution"] = function(...) end,
      --   ["eslint/openDoc"] = function(...) end,
      --   ["eslint/noLibrary"] = function(...) end,
      --   ["eslint/probeFailed"] = function(...) end,
      -- },
      settings = {
        -- Configuraciones específicas de ESLint
      },
    })

    lspconfig.yamlls.setup({
      on_attach = on_attach,
      capabilities = capabilities,
      settings = {
        yaml = {
          schemas = {
            ["https://raw.githubusercontent.com/quantumblacklabs/kedro/develop/static/jsonschema/kedro-catalog-0.17.json"] = "conf/**/*catalog*",
            ["https://json.schemastore.org/github-workflow.json"] = "/.github/workflows/*",
          },
        },
      },
    })

    lspconfig.templ.setup({
      capabilities = capabilities,
      on_attach = on_attach,
    })

    lspconfig.htmx.setup({
      on_attach = on_attach,
      capabilities = capabilities,
      filetypes = { "html", "templ" },
    })
  end,
}
