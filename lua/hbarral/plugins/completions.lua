return {
  "saghen/blink.cmp",
  -- optional: provides snippets for the snippet source
  dependencies = {
    "rafamadriz/friendly-snippets",
    "echasnovski/mini.icons",
    "nvim-tree/nvim-web-devicons",
    "onsails/lspkind.nvim",
  },

  -- use a release tag to download pre-built binaries
  version = "*",
  -- AND/OR build from source, requires nightly: https://rust-lang.github.io/rustup/concepts/channels.html#working-with-nightly-rust
  -- build = 'cargo build --release',
  -- If you use nix, you can build from source using latest nightly rust with:
  -- build = 'nix run .#build-plugin',

  ---@module 'blink.cmp'
  ---@type blink.cmp.Config
  opts = {
    -- 'default' for mappings similar to built-in completion
    -- 'super-tab' for mappings similar to vscode (tab to accept, arrow keys to navigate)
    -- 'enter' for mappings similar to 'super-tab' but with 'enter' to accept
    -- See the full "keymap" documentation for information on defining your own keymap.
    keymap = {
      preset = "none",
      ["<C-space>"] = { "show", "show_documentation", "hide_documentation" },
      ["<C-e>"] = { "hide", "fallback" },

      ["<Tab>"] = {
        function(cmp)
          if cmp.snippet_active() then
            return cmp.accept()
          else
            -- return cmp.select_and_accept()
            return cmp.select_next()
          end
        end,
        "snippet_forward",
        "fallback",
      },
      ["<S-Tab>"] = {
        function(cmp)
          if cmp.snippet_active() then
            return cmp.accept()
          else
            -- return cmp.select_and_accept()
            return cmp.select_prev()
          end
        end,
        "snippet_backward",
        "fallback",
      },

      ["<CR>"] = { "accept", "fallback" },

      ["<Up>"] = { "select_prev", "fallback" },
      ["<Down>"] = { "select_next", "fallback" },
      ["<C-p>"] = { "select_prev", "fallback" },
      ["<C-n>"] = { "select_next", "fallback" },

      ["<C-k>"] = { "select_prev", "fallback" },
      ["<C-j>"] = { "select_next", "fallback" },

      ["<C-b>"] = { "scroll_documentation_up", "fallback" },
      ["<C-f>"] = { "scroll_documentation_down", "fallback" },
    },

    appearance = {
      -- Sets the fallback highlight groups to nvim-cmp's highlight groups
      -- Useful for when your theme doesn't support blink.cmp
      -- Will be removed in a future release
      use_nvim_cmp_as_default = true,
      -- Set to 'mono' for 'Nerd Font Mono' or 'normal' for 'Nerd Font'
      -- Adjusts spacing to ensure icons are aligned
      nerd_font_variant = "mono",
    },

    -- Default list of enabled providers defined so that you can extend it
    -- elsewhere in your config, without redefining it, due to `opts_extend`
    sources = {
      default = { "lsp", "path", "snippets", "buffer" },
      -- Disable cmdline completions
      per_filetype = { sql = { "dadbod" } },
      providers = {
        dadbod = {
          name = "Dadbod",
          module = "vim_dadbod_completion.blink",
        },
        lsp = {
          name = "LSP",
          module = "blink.cmp.sources.lsp",
          opts = {}, -- Passed to the source directly, varies by source

          --- NOTE: All of these options may be functions to get dynamic behavior
          --- See the type definitions for more information
          enabled = true, -- Whether or not to enable the provider
          async = false, -- Whether we should wait for the provider to return before showing the completions
          timeout_ms = 2000, -- How long to wait for the provider to return before showing completions and treating it as asynchronous
          transform_items = nil, -- Function to transform the items before they're returned
          should_show_items = true, -- Whether or not to show the items
          max_items = nil, -- Maximum number of items to display in the menu
          min_keyword_length = 0, -- Minimum number of characters in the keyword to trigger the provider
          -- If this provider returns 0 items, it will fallback to these providers.
          -- If multiple providers falback to the same provider, all of the providers must return 0 items for it to fallback
          fallbacks = {},
          score_offset = 0, -- Boost/penalize the score of the items
          override = nil, -- Override the source's functions
        },
      },
    },

    -- Use a preset for snippets, check the snippets documentation for more information
    -- snippets = { preset = 'default' | 'luasnip' | 'mini_snippets' },

    -- signature = { window = { border = "single" } },
    completion = {
      menu = {
        draw = {
          treesitter = { "lsp" },
          columns = {
            { "kind_icon" },
            {
              "label",
              "label_description",
              gap = 1,
            },
            {
              "source_name",
            },
          },
          components = {
            kind_icon = {
              ellipsis = true,
              text = function(ctx)
                local kind_icon, _, _ = require("mini.icons").get("lsp", ctx.kind)
                return kind_icon
              end,
              -- Optionally, you may also use the highlights from mini.icons
              highlight = function(ctx)
                local _, hl, _ = require("mini.icons").get("lsp", ctx.kind)
                return hl
              end,
            },

            -- kind_icon = {
            --   ellipsis = true,
            -- text = function(ctx)
            --   local lspkind = require("lspkind")
            --   local icon = ctx.kind_icon
            --   if vim.tbl_contains({ "Path" }, ctx.source_name) then
            --     local dev_icon, _ = require("nvim-web-devicons").get_icon(ctx.label)
            --     if dev_icon then
            --       icon = dev_icon
            --     end
            --   else
            --     icon = require("lspkind").symbolic(ctx.kind, {
            --       mode = "symbol",
            --     })
            --   end
            --
            --   return icon .. ctx.icon_gap
            -- end,

            -- Optionally, use the highlight groups from nvim-web-devicons
            -- You can also add the same function for `kind.highlight` if you want to
            -- keep the highlight groups in sync with the icons.
            -- highlight = function(ctx)
            --   local hl = ctx.kind_hl
            --   if vim.tbl_contains({ "Path" }, ctx.source_name) then
            --     local dev_icon, dev_hl = require("nvim-web-devicons").get_icon(ctx.label)
            --     if dev_icon then
            --       hl = dev_hl
            --     end
            --   end
            --   return hl
            -- end,
            --   },
          },
        },
      },
      -- menu = {
      --   draw = {
      --     components = {
      --       kind_icon = {
      --         ellipsis = false,
      --         text = function(ctx)
      --           local kind_icon, _, _ = require("mini.icons").get("lsp", ctx.kind)
      --           return kind_icon
      --         end,
      --         -- Optionally, you may also use the highlights from mini.icons
      --         highlight = function(ctx)
      --           local _, hl, _ = require("mini.icons").get("lsp", ctx.kind)
      --           return hl
      --         end,
      --       },
      --     },
      --   },
      -- },
      -- menu = {
      --   border = "single",
      --   draw = {
      --     columns = { { "item_idx" }, { "kind_icon" }, { "label", "label_description", gap = 1 } },
      --     components = {
      --       item_idx = {
      --         text = function(ctx)
      --           return ctx.idx == 10 and "0" or ctx.idx >= 10 and " " or tostring(ctx.idx)
      --         end,
      --         highlight = "BlinkCmpItemIdx", -- optional, only if you want to change its color
      --       },
      --     },
      --   },
      -- },
      documentation = { window = { border = "single" } },
      -- 'prefix' will fuzzy match on the text before the cursor
      -- 'full' will fuzzy match on the text before *and* after the cursor
      -- example: 'foo_|_bar' will match 'foo_' for 'prefix' and 'foo__bar' for 'full'
      keyword = { range = "full" },

      -- Disable auto brackets
      -- NOTE: some LSPs may add auto brackets themselves anyway
      accept = { auto_brackets = { enabled = false } },

      trigger = { show_in_snippet = false },

      -- Don't select by default, auto insert on selection
      list = { selection = { preselect = false, auto_insert = false } },
      -- or set either per mode via a function
      -- list = { selection = {
      --   preselect = function(ctx)
      --     return ctx.mode ~= "cmdline"
      --   end,
      -- } },
      -- list = {
      --   selection = {
      --     preselect = function(ctx)
      --       return not require("blink.cmp").snippet_active({ direction = 1 })
      --     end,
      --   },
      -- },
    },
  },
  opts_extend = { "sources.default" },
}
