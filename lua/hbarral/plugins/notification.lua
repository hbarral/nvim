return {
  "rcarriga/nvim-notify",
  init = function()
    require("notify").setup({
      background_colour = "#000000",
      timeout = 1000,
      -- max_height = function()
      --   return math.floor(vim.o.lines * 0.75)
      -- end,
      -- max_width = function()
      --   return math.floor(vim.o.columns * 0.75)
      -- end,

      stages = "fade_in_slide_out",

      -- on_open = function(win)
      --   vim.api.nvim_win_set_config(win, {
      --     border = "rounded",
      --   })
      -- end,

      -- on_close = function(win)
      --   vim.api.nvim_win_close(win, true)
      -- end,

      top_down = true,
      render = "compact",
      icons = {
        DEBUG = "",
        ERROR = "",
        INFO = "",
        TRACE = "✎",
        WARN = "",
      },

      -- level = vim.log.levels.INFO,
      -- log_level = vim.log.levels.INFO,

      on_init = function()
        vim.notify = require("notify")
      end,
    })

    vim.notify = require("notify")
  end,
}
