return {
  "preservim/tagbar",
  config = function()
    vim.g.tagbar_autofocus = true
    vim.g.tagbar_compact = true
    vim.g.tagbar_autoclose = true
    vim.g.tagbar_show_linenumbers = 0
    vim.g.tagbar_iconchars = { "▶", "▼" }
    -- vim.keymap.set("n", "<F2>", "<cmd>TagbarToggle<CR>", { desc = "Toggle tagbar" })
  end,
}
