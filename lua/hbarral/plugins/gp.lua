return {
  "robitx/gp.nvim",
  config = function()
    local conf = {
      providers = {
        deepseek = {
          endpoint = "https://api.deepseek.com/v1/chat/completions",
          secret = os.getenv("DEEPSEEK_API_KEY"),
        },
        openai = {
          endpoint = "https://api.openai.com/v1/chat/completions",
          secret = os.getenv("OPENAI_API_KEY"),
        },
        googleai = {
          endpoint = "https://generativelanguage.googleapis.com/v1beta/models/{{model}}:streamGenerateContent?key={{secret}}",
          secret = os.getenv("GOOGLEAI_API_KEY"),
        },
        ollama = {
          endpoint = "http://localhost:11434/v1/chat/completions",
        },
        grok = {
          disable = false,
          endpoint = "https://api.x.ai/v1/chat/completions",
          secret = os.getenv("GROK_API_KEY"),
        },
        anthropic = {
          endpoint = "https://api.anthropic.com/v1/messages",
          secret = os.getenv("ANTHROPIC_API_KEY"),
        },
      },
      agents = {
        {
          name = "ChatGPT3-5",
          disable = true,
        },
        {
          name = "GrokAgent",
          provider = "grok",
          chat = true,
          command = true,
          model = { model = "grok-beta" },
          system_prompt = require("gp.defaults").chat_system_prompt,
        },
        {
          name = "O1Mini",
          provider = "openai",
          chat = true,
          command = true,
          model = { model = "o1-mini" },
          system_prompt = require("gp.defaults").chat_system_prompt,
        },
        {
          name = "DeepSeekChat",
          provider = "deepseek",
          chat = true,
          command = true,
          model = { model = "deepseek-chat" },
          system_prompt = require("gp.defaults").chat_system_prompt,
        },
      },
    }
    require("gp").setup(conf)
  end,
}
