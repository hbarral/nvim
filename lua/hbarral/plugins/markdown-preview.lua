return {
  "iamcco/markdown-preview.nvim",
  cmd = { "MarkdownPreviewToggle", "MarkdownPreview", "MarkdownPreviewStop" },
  ft = { "markdown" },
  -- build = function() vim.fn["mkdp#util#install"]() end,
  build = "cd app && yarn install",
  config = function()
    vim.g.mkdp_filetypes = { "markdown" }
    vim.g.mkdp_browser = "nyxt"
    vim.g.mkdp_auto_start = 1
    vim.g.mkdp_auto_close = 1

    vim.keymap.set("n", "<leader>m", "<cmd>MarkdownPreview<CR>", { desc = "Open markdown preview" })
  end,
}
