return {
  "mfussenegger/nvim-dap",
  lazy = true,
  dependencies = {
    "jay-babu/mason-nvim-dap.nvim",
    config = function()
      require("mason-nvim-dap").setup({ ensure_installed = { "firefox", "node2" } })
    end,
    -- "theHamsta/nvim-dap-virtual-text",
    "rcarriga/nvim-dap-ui",
    "nvim-telescope/telescope-dap.nvim",
    "rcarriga/cmp-dap",
    {
      "rcarriga/nvim-dap-ui",
      dependencies = { "mfussenegger/nvim-dap", "nvim-neotest/nvim-nio" },
    },
    "mfussenegger/nvim-jdtls",
    {
      "leoluz/nvim-dap-go",
      ft = "go",
      -- config = function(_, opts)
      --   require("dap-go").setup(opts)
      --   -- require("core.utils").load_mappings("dap_go")
      -- end,
    },
  },
  cmd = {
    "DapContinue",
    "DapLoadLaunchJSON",
    "DapRestartFrame",
    "DapSetLogLevel",
    "DapShowLog",
    "DapStepInto",
    "DapStepOut",
    "DapStepOver",
    "DapTerminate",
    "DapToggleBreakpoint",
    "DapToggleRepl",
  },
  config = function()
    local dap = require("dap")
    -- local jdtls = require("jdtls")
    local dapui = require("dapui")
    local mason_registry = require("mason-registry")

    -- Go
    require("dap-go").setup()

    -- Setup Telescope dap extension
    -- local ok_telescope, telescope = pcall(require, "telescope")
    -- if ok_telescope then
    --   telescope.load_extension("dap")
    -- end

    dap.adapters.php = {
      type = "executable",
      command = "node",
      -- get the PHP Debug Adapter
      -- git clone https://github.com/xdebug/vscode-php-debug.git
      -- cd vscode-php-debug
      -- npm install && npm run build
      args = { os.getenv("HOME") .. "/vscode-php-debug/out/phpDebug.js" },
      -- args = {
      -- require("mason-registry").get_package("php-debug-adapter"):get_install_path() .. "/extension/out/phpDebug.js",
      -- }
    }

    dap.configurations.php = {
      {
        name = "run current script",
        type = "php",
        request = "launch",
        port = 9003,
        cwd = "${fileDirname}",
        program = "${file}",
        runtimeExecutable = "php",
      },
      {
        name = "listen for Xdebug local",
        type = "php",
        request = "launch",
        port = 9003,
      },
      {
        name = "listen for Xdebug docker",
        type = "php",
        request = "launch",
        port = 9003,
        pathMappings = {
          ["/var/www"] = "${workspaceFolder}",
        },
      },
    }

    dap.configurations.java = {
      {
        type = "java",
        request = "attach",
        name = "Attach to the process",
        hostName = "127.0.0.1",
        port = 8787,
      },
    }

    -- TRACE
    -- DEBUG
    -- INFO
    -- WARN
    -- ERROR
    dap.set_log_level("TRACE")

    dapui.setup({
      icons = { expanded = "▾", collapsed = "▸", current_frame = "»" },
      mappings = {
        -- Use a table to apply multiple mappings
        expand = { "<CR>", "<2-LeftMouse>" },
        open = "o",
        remove = "d",
        edit = "e",
        repl = "r",
        toggle = "t",
      },
      layouts = {
        {
          elements = {
            "scopes",
            -- 'breakpoints',
            "stacks",
            -- 'watches',
          },
          size = 40,
          position = "left",
        },
        {
          elements = {
            "repl",
            --    'console',
          },
          size = 10,
          position = "bottom",
        },
      },
      floating = {
        max_height = nil, -- These can be integers or a float between 0 and 1.
        max_width = nil, -- Floats will be treated as percentage of your screen.
        border = "rounded", -- Border style. Can be "single", "double" or "rounded"
        mappings = {
          close = { "q", "<Esc>" },
        },
      },
      controls = {
        -- Requires Neovim nightly (or 0.8 when released)
        enabled = false, -- because the icons don't work
        -- Display controls in this element
        element = "repl",
        icons = {
          pause = "",
          play = "",
          step_into = "",
          step_over = "",
          step_out = "",
          step_back = "",
          run_last = "",
          terminate = "",
        },
      },
      windows = { indent = 1 },
    })

    -- add listeners to auto open DAP UI
    -- dap.listeners.after.event_initialized["dapui_config"] = function()
    --   dapui.open()
    -- end
    -- dap.listeners.before.event_terminated["dapui_config"] = function()
    --   dapui.close()
    -- end
    -- dap.listeners.before.event_exited["dapui_config"] = function()
    --   dapui.close()
    -- end

    dap.listeners.before.attach.dapui_config = function()
      dapui.open()
    end
    dap.listeners.before.launch.dapui_config = function()
      dapui.open()
    end
    dap.listeners.before.event_terminated.dapui_config = function()
      dapui.close()
    end
    dap.listeners.before.event_exited.dapui_config = function()
      dapui.close()
    end

    vim.fn.sign_define(
      "DapBreakpoint",
      { text = "⏺", texthl = "DapBreakpoint", linehl = "DapBreakpoint", numhl = "DapBreakpoint" }
    )

    dap.adapters.go = {
      type = "executable",
      command = "node",
      args = { os.getenv("HOME") .. "/apps/vscode-go/extension/dist/debugAdapter.js" },
    }

    dap.configurations.go = {
      {
        type = "go",
        name = "Debug",
        request = "launch",
        showLog = true,
        program = "${file}",
        dlvToolPath = vim.fn.exepath("dlv"), -- Adjust to where delve is installed
        -- /home/hbarral/go/bin/dlv
      },
    }

    -- Setup Virtual Text
    -- require("nvim-dap-virtual-text").setup({})

    -- Mappings
    vim.keymap.set("n", "<F5>", function()
      require("dap").continue()
    end)
    vim.keymap.set("n", "<leader>da", function()
      require("dap").continue()
    end)

    vim.keymap.set("n", "<F10>", function()
      require("dap").step_over()
    end)
    vim.keymap.set("n", "<F11>", function()
      require("dap").step_into()
    end)
    -- vim.keymap.set('n', 'S-<F12>', function() require('dap').step_out() end)
    -- vim.keymap.set('n', '<Leader>b', function() require('dap').toggle_breakpoint() end)
    vim.keymap.set("n", "<Leader>B", function()
      require("dap").set_breakpoint()
    end)
    vim.keymap.set("n", "<Leader>lp", function()
      require("dap").set_breakpoint(nil, nil, vim.fn.input("Log point message: "))
    end)
    vim.keymap.set("n", "<Leader>dr", function()
      require("dap").repl.open()
    end)
    vim.keymap.set("n", "<Leader>dl", function()
      require("dap").run_last()
    end)
    vim.keymap.set({ "n", "v" }, "<Leader>dh", function()
      require("dap.ui.widgets").hover()
    end)
    vim.keymap.set({ "n", "v" }, "<Leader>dp", function()
      require("dap.ui.widgets").preview()
    end)
    vim.keymap.set("n", "<Leader>df", function()
      local widgets = require("dap.ui.widgets")
      widgets.centered_float(widgets.frames)
    end)
    vim.keymap.set("n", "<Leader>ds", function()
      local widgets = require("dap.ui.widgets")
      widgets.centered_float(widgets.scopes)
    end)
  end,
}
