return {
  "voldikss/vim-floaterm",
  config = function()
    vim.g.floaterm_autoclose = 1
    vim.g.floaterm_width = 0.8
    vim.g.floaterm_height = 0.8
    vim.g.floaterm_title = "HB ($1/$2)"

    vim.cmd([[ highlight Floaterm guibg=NONE ]])
    vim.cmd([[ highlight FloatermBorder guibg=NONE guifg=#88c0d0 ]])
    vim.cmd([[ highlight FloatermNC guibg=gray ]])

    -- Add Server command for make down && make up in floaterm
    vim.cmd([[ command! Server FloatermNew --width=0.8 --height=0.8 --autoclose=1 make down && make up ]])

    -- Add Compile command for make build && make down in floaterm
    vim.cmd([[ command! Compile FloatermNew --width=0.8 --height=0.8 --autoclose=1 make build && make down ]])
  end,
}
