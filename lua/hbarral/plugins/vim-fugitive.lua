return {
	"tpope/vim-fugitive",
	config = function()
		vim.keymap.set("n", "<Leader>ga", ":Gwrite<CR>")
		vim.keymap.set("n", "<Leader>gc", ":Gcommit<CR>")
		vim.keymap.set(
			"n",
			"<Leader>gP",
			":Git push<CR>",
			{ silent = true, noremap = true, desc = "Upload commits to the repository" }
		)
		vim.keymap.set("n", "<Leader>gll", ":Gpull<CR>")
		vim.keymap.set("n", "<Leader>gs", ":Git<CR> | <C-w>J")
		vim.keymap.set("n", "<Leader>gb", ":Git blame<CR>")
		vim.keymap.set("n", "<Leader>gd", ":Gvdiff<CR>")
		vim.keymap.set("n", "<Leader>gr", ":GRemove<CR>")
	end,
}
