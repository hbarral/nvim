return {
  "smoka7/hop.nvim",
  version = "*",
  opts = {
    keys = "etovxqpdygfblzhckisuran",
    multi_windows = true,
    case_insensitive = true,
    create_hl_autocmd = true,
  },
  config = function()
    local hop = require("hop")
    hop.setup()

    -- Saltar a un carácter específico (similar a 'f' pero en todo el buffer)
    vim.keymap.set({ "n", "v" }, "<leader>hh", function()
      hop.hint_char1()
    end, { desc = "Hop char" })

    -- Saltar a una palabra
    vim.keymap.set({ "n", "v" }, "<leader>hw", function()
      hop.hint_words()
    end, { desc = "Hop word" })

    -- Saltar a una línea
    vim.keymap.set({ "n", "v" }, "<leader>hl", function()
      hop.hint_lines()
    end, { desc = "Hop line" })

    -- Saltar a un patrón
    vim.keymap.set({ "n", "v" }, "<leader>hp", function()
      hop.hint_patterns()
    end, { desc = "Hop pattern" })

    -- Saltar al inicio de una línea
    vim.keymap.set({ "n", "v" }, "<leader>h^", function()
      hop.hint_lines_skip_whitespace()
    end, { desc = "Hop line start" })

    -- Saltar verticalmente
    vim.keymap.set({ "n", "v" }, "<leader>hv", function()
      hop.hint_vertical()
    end, { desc = "Hop vertical" })
  end,
}
