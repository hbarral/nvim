return {
  "nvim-lualine/lualine.nvim",
  dependencies = {
    "nvim-tree/nvim-web-devicons",
    "archibate/lualine-time",
    requires = { "kyazdani42/nvim-web-devicons", opt = true },
  },
  config = function()
    local lualine = require("lualine")
    -- local lazy_status = require("lazy.status")

    lualine.setup({
      options = {
        icons_enabled = true,
        theme = "auto",
        component_separators = { left = "", right = "" }, -- , ,
        section_separators = { left = "", right = "" }, -- , 
        disabled_filetypes = {
          statusline = {},
          winbar = {},
        },
        ignore_focus = { "NvimTree" },
        always_divide_middle = true,
        globalstatus = false,
        refresh = {
          statusline = 1000,
          tabline = 1000,
          winbar = 1000,
        },
      },
      sections = {
        lualine_a = {
          "filename",
        },
        -- lualine_b = { "diff", "diagnostics" },
        lualine_b = {},
        -- lualine_c = { "branch" },
        lualine_c = {},
        -- lualine_x = {
        -- 	{
        -- 		lazy_status.updates,
        -- 		cond = lazy_status.has_updates,
        -- 	},
        -- 	{ "encoding" },
        -- 	{ "fileformat" },
        -- 	{ "filetype" },
        -- },
        lualine_x = {},
        -- lualine_y = { "progress" },
        lualine_y = {},
        -- lualine_z = {
        --   { "location" },
        --   -- { "cdate" },
        --   { "ctime" },
        -- },
        lualine_z = {},
      },
      inactive_sections = {
        lualine_a = {},
        lualine_b = {},
        lualine_c = { "filename" },
        lualine_x = { "location" },
        lualine_y = {},
        lualine_z = {},
      },
      tabline = {},
      winbar = {},
      inactive_winbar = {},
      extensions = {
        "fern",
        "fugitive",
        "man",
        "nvim-tree",
        "quickfix",
      },
    })
  end,
}
