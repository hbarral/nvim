return {
  "nvimtools/none-ls.nvim",
  dependencies = {
    "nvim-lua/plenary.nvim",
    "nvimtools/none-ls-extras.nvim",
  },
  config = function()
    local null_ls = require("null-ls")
    local formatting = null_ls.builtins.formatting
    local diagnostics = null_ls.builtins.diagnostics
    -- local completion = null_ls.builtins.completion
    local augroup = vim.api.nvim_create_augroup("LspFormatting", {})

    null_ls.setup({
      debug = false,
      sources = {
        -- lua
        formatting.stylua.with({ extra_args = { "--indent-type", "Spaces", "--indent-width", "2" } }),

        -- go
        formatting.gofumpt,
        formatting.goimports,
        formatting.goimports_reviser,

        -- formatting.prettier.with({
        --   -- extra_filetypes = { "toml" },
        --   extra_args = { "--no-semi", "--single-quote", "--jsx-single-quote" },
        -- }),
        -- completion.spell,

        -- javascript
        -- diagnostics.eslint_d,
        -- formatting.eslint_d,
        -- require("none-ls.diagnostics.eslint_d"),
        -- formatting.eslint_d,
        require("none-ls.diagnostics.eslint_d").with({
          "--fix-dry-run",
          "--format",
          "json",
          "--stdin",
          "--stdin-filename",
          "$PATH_TO_FILE",
        }),

        -- python
        formatting.black.with({
          extra_args = { "--fast", "--line-length=88", "--skip-string-normalization" },
        }),
        formatting.isort,

        -- json
        -- diagnostics.spectral,
        -- formatting.biome,
        diagnostics.cfn_lint,
        -- diagnostics.spectral.with({ "lint", "--stdin-filepath", "$FILENAME", "-f", "json" }),
        -- formatting.prettier,
        formatting.prettierd,
        -- docker
        diagnostics.hadolint,

        -- java
        -- formatting.google_java_format,

        -- xml
        formatting.xmllint,

        -- sql
        formatting.sql_formatter,
      },
      on_attach = function(client, bufnr)
        if client.supports_method("textDocument/formatting") then
          vim.api.nvim_clear_autocmds({
            group = augroup,
            buffer = bufnr,
          })
          vim.api.nvim_create_autocmd("BufWritePre", {
            group = augroup,
            buffer = bufnr,
            callback = function()
              vim.lsp.buf.format({ bufnr = bufnr })
            end,
          })
        end
      end,
    })
  end,
}
