return {
  "hbarral/vsession",
  config = function()
    vim.opt.viewoptions:append({
      "cursor",
      -- 'folds',
      "slash",
      -- 'unix',
    })

    vim.opt.viewoptions:remove({
      "options",
    })

    vim.opt.sessionoptions:remove({
      "options",
      "folds",
    })

    -- vim.g.vsession_path = '~/.sessions'
    vim.g.vsession_save_last_on_exit = 1
    vim.g.vsession_save_last_on_leave = 1
    vim.g.vsession_use_fzf = 1

    -- " allowed values are 'quickpick' or 'fzf' or 'popup' or 'input'.
    vim.g.vsession_ui = "fzf"

    vim.keymap.set("n", "<leader>so", ":LoadSession<CR>")
    vim.keymap.set("n", "<leader>ss", ":SaveSession<CR>")
    vim.keymap.set("n", "<leader>sd", ":DeleteSession<CR>")
  end,
}
