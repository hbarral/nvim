return {
  'lambdalisue/fern.vim',
  dependencies = {
    'lambdalisue/nerdfont.vim',
    'lambdalisue/fern-renderer-nerdfont.vim',
    'lambdalisue/fern-hijack.vim',
    'lambdalisue/vim-fern-git-status',
  },
  config = function()
    local keymap = vim.keymap
    local opts = { noremap = true, silent = true }
    local augroup = vim.api.nvim_create_augroup
    local autocmd = vim.api.nvim_create_autocmd

    keymap.set("n", "<space>e", "<cmd>Fern . -reveal=% -drawer -toggle <CR>", opts)

    vim.g['fern#renderer']                       = 'nerdfont'
    vim.g['fern_git_status#disable_ignored']     = 1
    vim.g['fern_git_status#disable_untracked']   = 1
    vim.g['fern_git_status#disable_submodules']  = 1
    vim.g['fern_git_status#disable_directories'] = 1
    vim.g['fern#drawer_width']                   = 30
    vim.g['fern#default_hidden']                 = 1
    vim.g['fern#disable_drawer_smart_quit']      = 0
    vim.g['fern#hide_cursor']                    = 1

    augroup('fern-custom', { clear = true })
    autocmd('FileType', {
      group = 'fern-custom',
      pattern = 'fern',
      callback = function()
        keymap.set("n", "H", "<Plug>(fern-action-open:split)", { buffer = true })
        keymap.set("n", "V", "<Plug>(fern-action-open:vsplit)", { buffer = true })
        keymap.set("n", "R", "<Plug>(fern-action-rename)", { buffer = true })
        keymap.set("n", "M", "<Plug>(fern-action-move)", { buffer = true })
        keymap.set("n", "C", "<Plug>(fern-action-copy)", { buffer = true })
        keymap.set("n", "N", "<Plug>(fern-action-new-path)", { buffer = true })
        keymap.set("n", "dd", "<Plug>(fern-action-trash)", { buffer = true })
        keymap.set("n", "m", "<Plug>(fern-action-mark)", { buffer = true })

        vim.opt.relativenumber = false
        vim.opt.cursorline = true
        vim.opt.number = false
      end
    })
  end
}
