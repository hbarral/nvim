return {
  "L3MON4D3/LuaSnip",
  config = function()
    require("luasnip.loaders.from_vscode").lazy_load()
    require("luasnip.loaders.from_snipmate").load({ path = { "~/.config/nvim/snippets/snipmate" } })
    require("luasnip.loaders.from_lua").load({ paths = "~/.config/nvim/snippets/luasnip" })
  end,
}
