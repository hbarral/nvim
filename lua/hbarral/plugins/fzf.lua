return {
  "junegunn/fzf",
  dependencies = {
    "junegunn/fzf.vim",
  },
  config = function()
    vim.env.FZF_DEFAULT_COMMAND = "rg --files --hidden"

    local function rg_fzf(args, bang)
      local rg_command =
      "rg -g '!node_modules/' -g '!target/' -g '!dist/' -g '!dockerfiles/' -g '!ios/' -g '!android/' -g '!/tags' -g '!vendor/' -g '!yarn.lock' -g '!package-lock.json' -g '!composer.lock' --column --line-number --no-heading --color=always --smart-case "
      rg_command = rg_command .. vim.fn.shellescape(args)
      vim.call("fzf#vim#grep", rg_command, 1, { options = "--delimiter : --nth 4.." }, bang)
    end

    vim.api.nvim_create_user_command("RG", function(opts)
      rg_fzf(opts.args, opts.bang)
    end, { nargs = "*", bang = true })
  end,
}
