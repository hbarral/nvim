local jdtls_ok, jdtls_config = pcall(require, "hbarral.core.jdtls")
if not jdtls_ok then
  vim.notify("Failed to load JDTLS config", vim.log.levels.ERROR)
  return
end

local config = jdtls_config.get_config()
if config then
  require("jdtls").start_or_attach(config)
end
